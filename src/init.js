/*
| Initializes the ti2c-log package.
*/
await ti2c.register(
	'name',    'log',
	'meta',    import.meta,
	'source',  'src/',
	'relPath', 'init'
);
