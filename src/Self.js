/*
| The ti2c-log utility.
*/
def.attributes =
{
	// the level to log at.
	//
	// levels are:
	//    debug:
	//    normal:
	//    error:
	level: { type: 'string', defaultValue: 'normal' },

	// prefix log messages for this instance (e.g. a connection identifier)
	prefix: { type: [ 'undefined', 'string' ] },
};

/*
| FIXME.
*/
def.proto.debug =
	function( ...args )
{
	if( this._iLevel <= 0 )
	{
		if( this.prefix ) args.unshift( prefix );
		console.log.apply( args );
	}
};

/*
| FIXME.
*/
def.proto.normal =
	function( ...args )
{
	if( this._iLevel <= 1 )
	{
		if( this.prefix ) args.unshift( prefix );
		console.log.apply( args );
	}
};

/*
| FIXME.
*/
def.proto.error =
	function( ...args )
{
	if( this._iLevel <= 1 )
	{
		if( this.prefix ) args.unshift( prefix );
		console.log.apply( args );
	}
};

/*
| FIXME.
*/
def.lazy._iLevel =
	function( )
{
	switch( this.level )
	{
		case 'debug':  return 0;
		case 'normal': return 1;
		case 'error':  return 2;
		default: throw new Error( );
	}
};
